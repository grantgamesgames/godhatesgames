import Head from 'next/head'
import React from 'react';
import Image from 'next/image';
import { Content } from 'antd/lib/layout/layout';
import { Typography, Button, Row, Space, Col } from 'antd';
import { AmazonOutlined } from '@ant-design/icons';
import styles from '../styles/index.module.css'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

export default function Home() {
  // const classes = useStyles();

  return (
    <React.Fragment>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Content className={styles.logoContainer}>
        <Row justify="center">
          <Col>
            <Image src="/god_hates_games_logo_AW_white-02.png" alt="God Hates Games" layout="intrinsic" width="384.5" height="307" />
          </Col>
        </Row>
        <Row justify="center">
          <Col>
            <Button 
              ghost={true}
              type="default" 
              shape="round" 
              icon={<AmazonOutlined />} 
              href="https://amzn.to/3uHvbme"
            >Buy&nbsp;<b>God Hates Charades</b></Button>
          </Col>
        </Row>
        
      </Content>
    </React.Fragment>
  );
}